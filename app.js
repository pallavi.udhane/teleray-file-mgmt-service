var express = require('express');
var timeout = require('connect-timeout')
var cors = require('cors')
var path = require('path');
var favicon = require('serve-favicon');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var patientstudy = require('./model/patient_studies');
var patientstudies = require('./routes/patientstudy');
var logger = require('./config/logger');
var config = require('config');
var functionCheck = require('./routes/functionCheck');
var fileupload = require('./routes/fileupload');
var app = express();
var dbSecret = '';
var dbConnection = '';

var router = express.Router();

		

if (config.has('tokenConfig.secret')) {
    dbSecret = config.get('tokenConfig.secret');
}
if (config.has('dbConfig.host') && config.has('dbConfig.port') && config.has('dbConfig.databasename')) {

    dbConnection = config.get('dbConfig.host')+':'+config.get('dbConfig.port')+'/'+config.get('dbConfig.databasename');
    
}

//db.connect(dbConfig, ...);

mongoose.connect(dbConnection); //connect to the database
app.set('superSecret', dbSecret);
console.log('creating connection to ' + dbConnection);
logger.info('database connection get created for teleray');


// view engine setup	
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.get('/', function(req, res){
    res.render("index");
});


//app.use(express.static(__dirname +  '/views'));





//app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({
    limit: '200mb'
}));
app.use(bodyParser.urlencoded({
    limit: '200mb',
    extended: true
}));


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/teleray-file-mgmt-service/patientstudies', patientstudies);
app.use('/teleray-file-mgmt-service/functionCheck', functionCheck);
app.use('/teleray-file-mgmt-service/fileupload', fileupload);

//app.use('/teleray-file-mgmt-service', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;