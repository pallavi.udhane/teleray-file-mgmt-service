  var express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'), //mongo connection
      bodyParser = require('body-parser'), //parses information from POST
      methodOverride = require('method-override'),
      jwt = require('jsonwebtoken'),
      mongoose = require('mongoose'), //mongo connection

      RBAC = require('rbac2'),
      config = require('config'),
      request = require('request-json'),
      logger = require('../config/logger');


  router.use(bodyParser.urlencoded({
      extended: true
  }))
  router.use(methodOverride(function(req, res) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method
          delete req.body._method
          return method
      }
  }))


  var app = express();
  var dbSecret = '';
  if (config.has('tokenConfig.secret')) {
      dbSecret = config.get('tokenConfig.secret');
  }
  app.set('superSecret', dbSecret);


	var baseTokenUrl;
 	if (config.has('medirazerbaseurltokenport')) {
  		baseTokenUrl = config.get('medirazerbaseurltokenport');
  	    console.log('getting medirazerbaseurltokenport url as :' + baseTokenUrl);
  	    logger.info('Inside tokenService :: getting baseTokenUrl as :' + baseTokenUrl);
  	}
  							
  	var tokenClient = request.createClient(baseTokenUrl); 

  var checkPermission =
      function(req, res, next) {
          logger.info('Inside functionCheck :: checkPermission');
          var rolename = req.headers['rolename'];          
          var actionname = req.headers['actionname'];    
          var rulesForRole = req.headers['rules'];
          logger.info('Inside functionCheck :: checkPermission::ROLENAME as :' + rolename + 'ACTIONNAME as ::' + actionname + 'RULESAS::' + rulesForRole);
          var rules = eval('(' + rulesForRole + ')');
          
          var rback = new RBAC(rules);
          
        rback.check(rolename, actionname, function(err, result) {
              // result: true 
             
              if (result) {
                   logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as :' +result);
                  next();
              } else {
                 logger.info('Inside functionCheck :: checkPermission::Inside rbackcheck result as false :' +err);
                  res.json({
                    
                      header: {
                          statuscode: 401,
                          statusmessage: "failure"
                      },
                      data: {
                          errormessage: 'Sorry you dont have permission for ' + actionname
                      }
                    
                  });

              }
          });



      };


  var checkToken = function(req, res , next) {
       logger.info('Inside functionCheck :: checkToken');
       console.log('Inside functionCheck :: checkToken');
      var token = null;
logger.info('FileMgmt::Inside functionCheck :: checkToken::getting dbSecret');
console.log('FileMgmt::Inside functionCheck :: checkToken::getting dbSecret');



     var bearerHeader = req.headers['authorization'];
     var bearer = null;
      if (typeof bearerHeader !== 'undefined') {
          var bearer = bearerHeader.split(" ");
          
          token = bearer[1];
          
          
      }     
      var data = {
    		  "token" : token
      }
      console.log('getting token for verification as :: ' +token);
      // decode token
      if (token) {
    	  //
    	  console.log('calling token client servie for token verification');
    	  tokenClient.post('functionCheck/verifyToken',data,function(err , clientRes , body) {
    		  console.log('verifyToken returning err as :' +JSON.stringify(err));
    		  console.log('verifyToken returning clientRes as :' +JSON.stringify(clientRes));
    		  var statusCode = clientRes.statusCode;
    		  console.log('statusCode' +statusCode);
    		  if(statusCode == '200' ){
    			  next();
    		  }else{
    	    	    return res.json({
    	                header: {
    	                    statuscode: 400,
    	                    statusmessage: "failure"
    	                },
    	                data: {
    	                    errormessage: "Failed to authenticate token" + err
    	                }

    	            });
    		  }
    		  console.log('verifyToken returning body as :' +JSON.stringify(body.data));
    		  
    		  
    	  });
      }
      else{
    	    return res.json({
              header: {
                  statuscode: 400,
                  statusmessage: "failure"
              },
              data: {
                  errormessage: "Failed to authenticate token" + err
              }

          });
      }
//      	tokenClient.post('functionCheck/checkToken',token, function(resCheckToken) {
//      	  //return console.log('status message :'+res+'status code ::' +res.statusCode);
//      	  console.log('retruning after aclling checkToken');
//      	  console.log('getting token as ::' +resCheckToken);
//      	  if(resCheckToken){
//      		  next();
//      	  }else{
//      	    return res.json({
//                header: {
//                    statuscode: 400,
//                    statusmessage: "failure"
//                },
//                data: {
//                    errormessage: "Failed to authenticate token" + err
//                }
//
//            });
//      	  }
//      	});
//      } else {
//
//          // if there is no token
//          // return an error
//              return res.json({
//                      header: {
//                          statuscode: 403,
//                          statusmessage: "failure"
//                      },
//                      data: {
//                          errormessage: "No token provided."
//                      }
//                  });
//      }
  };

  module.exports = router;
  module.exports.checkPermission = checkPermission;
  module.exports.checkToken = checkToken;